class CloseButton {
	constructor() {
		this.game = window.game;
		this.orient = 'portrait';
		this.timer = 15;
		this.initCloseButton ();
		this.initCountDown();
	}

	show () {
		this.cb.style.display = "block";
	}

	hide () {
		this.cb.style.display = "none";

		if (this.countDown) {
			this.countDown.style.display = "none";
		}
	}

	initCloseButton () {
		var cb = window.document.createElement('img');
		cb.style.width = "3vmax";
		cb.style.height = "3vmax";
		cb.style.margin = "0";
		cb.style.position = "fixed";
		cb.style.display = "none";
		cb.src = "./assets/closeButton.png";
		cb.onclick = this.closeAd

		setTimeout(function() {
			this.show ();
		}.bind(this), this.timer * 1000)

		window.gamebox.appendChild(cb);

		this.cb = cb;
	}

	initCountDown () {
		var div = window.document.createElement('div');

		div.setAttribute("id", "countDown");
		div.style.backgroundColor = "#000000";
		div.style.width = "2.75vmax";
		div.style.height = "2.75vmax";
		div.style.margin = "0";
		div.style.position = "fixed";
		div.style.display = "none";
		div.style.borderRadius = "3vmax";
		div.style.border = ".1vmax solid #a6a6a6";
		div.style.fontFamily = "Arial";
		div.style["text-align"] = "center";
		div.style.verticalAlign = "middle";
		div.style.color = "#a6a6a6";
		div.style.fontSize = "2vmax";
		div.style.lineHeight = "2.5vmax";
		div.innerText = this.timer;

		window.gamebox.appendChild(div);

		this.countDown = div;
	}

	startCountDown () {		
		this.countDown.style.display = "block";

		var seconds = this.timer;
		var countDownInterval = setInterval(function() {
			this.countDown.innerText = --seconds;
			if(seconds === 0) {
				clearInterval(countDownInterval);
				this.countDown.style.display = "none";
			}
		}.bind (this), 1 * 1000);
	}

	closeAd () {
		window.close();
	}

	reorient (orientation) {
		this.orient = orientation;

		var offset = "5";
		this.countDown.style.top = this.cb.style.top = offset + "px";
		this.countDown.style.left = this.cb.style.left = offset + "px";
		this.cb.style.padding = "0 15px 15px 0";
	}
}