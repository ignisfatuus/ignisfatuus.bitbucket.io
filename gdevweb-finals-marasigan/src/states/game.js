class Game extends Phaser.State {
    constructor() {
        super();
        
        this.config = {
            landscape: {
                background: {
                    x: game.baseResolution.max * 0.5,
                    y: game.baseResolution.min * 0.5, 
                    angle: 90
                },
                installButton: {
                    x: game.baseResolution.max * 0.5,
                    y: game.baseResolution.min * 0.1,
                    scaleX: 1,
                    scaleY: 1.2
                },
                endCard: {
                    x: game.baseResolution.max * 0.5,
                    y: game.baseResolution.min * 0.5,
                    scaleX: 1,
                    scaleY: 1.2
                },

                characterSprite :
                {

                    x: game.baseResolution.max * 0.375,
                    y: game.baseResolution.min * 0.5,
                    scaleX:0.050,
                    scaleY:0.050
                
                },

                pipeAmount:
                {
                    x: 80,
                    position : game.baseResolution.max *0.75
                },

                ground:
                {
                    y: game.baseResolution.min * 1.17,
                },
  
                endCardPicture:
                {
                    x:game.baseResolution.max *0.0005,
                    y:game.baseResolution.min *0.005,
                    scaleX: 1,
                    scaleY:0.5
                }
            },
            portrait: {
                background: {
                    x: game.baseResolution.min * 0.5,
                    y: game.baseResolution.max * 0.5,
                    angle: 0
                },
                installButton: {
                    x: game.baseResolution.min * 0.5,
                    y: game.baseResolution.max * 0.05,
                    scaleX: 1,
                    scaleY: 1
                },
                endCard: {
                    x: game.baseResolution.min * 0.5,
                    y: game.baseResolution.max * 0.5,
                    scaleX: 1,
                    scaleY: 1
                },

                characterSprite :
                {

                 
                    x: game.baseResolution.max * 0.15,
                    y: game.baseResolution.min * 0.75,
                    scaleX:0.075,
                    scaleY:0.075     
                },
                pipeAmount:
                {
                    x: 120,
                    position : game.baseResolution.min *0.75
                },

                ground:
                {
                    y: game.baseResolution.min * 1.55,
                },

                endCardPicture:
                {
                    x:game.baseResolution.max *0.0005,
                    y:game.baseResolution.min *0.005,
                    scaleX: 1,
                    scaleY:1
                }
                
            }
        }
    }

    preload() 
    {
        this.game.load.image('gameBg', 'assets/sprites/pre.png');
        this.game.load.image('characterImage','assets/sprites/pizzaCharacter.png')
        this.game.load.image('pipe', 'assets/sprites/pipe.png');
        this.game.load.image('groundSprite','assets/sprites/ground.png');
        this.game.load.image('roofSprite','assets/sprites/roof.png');
        this.game.load.image('endCard','assets/sprites/pweds.jpg')
    }

    
     hasGameStarted;
     tutorialText;
     score = -1;
     deathCount=0;
     pipeAmount;
     pipeXAxis;
     isDone = false;
    create() {

        //Add arcade physics
        game.physics.startSystem(Phaser.Physics.ARCADE);

        // game container (for resizing)
        game.container = game.world.addChild (new Phaser.Group (game));
        game.container.reorientLayout = this.reorientLayout.bind(this);

        // game container
        this.container = new Phaser.Group (game);
        game.container.add (this.container);

        // background
        this.background = this.container.add (this.game.add.graphics());
        this.background.beginFill (0x333333);
        this.background.drawRect (-game.baseResolution.min*0.5, -game.baseResolution.max*0.5, game.baseResolution.min, game.baseResolution.max);
        this.background.endFill ();
        this.background.anchor.setTo (0.5, 0.5);

        // install button at top
        this.installButton = new InstallButton (game);
        this.container.addChild (this.installButton);

        this.endCard = new EndCard (game);
        
        this.endCardPicture = this.game.add.sprite(game.baseResolution.max *0.56,game.baseResolution.max *0.10,'endCard');
        this.endCardPicture.anchor.setTo (0.5, 0.8);
        this.endCardPicture.visible=false;
        this.endCard.addChild(this.endCardPicture);
        //Add gameObjects

        //Background
        this.backgroundSprite = this.game.add.sprite(0,0,'gameBg');
        this.backgroundSprite.anchor.setTo(0.5,0.5);
        this.container.add(this.backgroundSprite);

        //Character Initialization
        this.characterSprite = this.game.add.sprite(100,250,'characterImage');
        this.characterSprite.anchor.setTo(0.6,0.5);
        this.characterSprite.scale.x = 0.05;
        this.characterSprite.scale.y = 0.05;

        this.container.add(this.characterSprite);

        this.ground = this.game.add.sprite(450,770,'groundSprite');
        this.ground.anchor.setTo(0.5,0.5);
        this.ground.scale.x *=10;
        this.container.add(this.ground);

        this.roof = this.game.add.sprite(450,-80,'roofSprite');
        this.roof.anchor.setTo(0.5,0.5);
        this.roof.scale.x *=10;
        this.container.add(this.roof);


     //   this.container.add(this.characterSprite);


        //Initialize Character Physics
       // game.physics.enable(this.characterSprite, Phaser.Physics.ARCADE);
       // this.characterSprite.body.gravity.y = 1000;
       // this.characterSprite.body.bounce.y = 0.5;
        // Initialize Controls
       // var spaceKey = game.input.keyboard.addKey(
       //     Phaser.Keyboard.SPACEBAR);
      ///  spaceKey.onDown.add(this.jump, this);  
 
     // this.InitializeCharacter();
        this.gameStart ();

       var style = 
       {
        font: 'Arial',
        fontSize: 32,
        fill: "#ff1a1a", 
        align: "center"
        };
    
        //Add tap screen text
   //    this.tutorialText =  (this.game.add.text (this.game.world.centerX, this.game.height * 0.25, "Tap the screen", style));
    //   this.tutorialText.anchor.setTo (0.5, 0.5);
     //   this.container.add(this.tutorialText);
       this.hasGameStarted = false;
       //Add pointer tutorial
     // var spaceKey = game.input.keyboard.addKey(
      //  Phaser.Keyboard.UP);
      //  spaceKey.onDown.add(this.PointerTutorial, this); 

       this.game.input.onDown.add(this.PointerTutorial,this);
        //Add pipes
        this.pipes = game.add.group();
       this.container.add(this.pipes);

        //add score and death count
     
        this.labelScore = game.add.text(this.game.world.centerX-150, 20, "0", 
    { font: "75px Arial", fill: "#000000" });  
         this.container.add(this.labelScore);

        // update orientation
        adaptGameToOrientation();
        assignOrientationChangeHandlers();

        
    }

    update()
    {

        game.physics.arcade.overlap(
            this.characterSprite, this.pipes, this.restartGame, null, this);


              
    }
    restartGame()
    {
        if(this.isDone) return;
        // remove pipes
        this.pipes.removeAll();
        this.hasGameStarted=false;
        this.characterSprite.x = 100;
        this.characterSprite.y = 250;
        game.time.events.remove(this.timer);
        this.score=-1;
        this.characterSprite.body.enable=false;
        this.deathCount+=1;
        var style = 
        {
         font: 'Arial',
         fontSize: 32,
         fill: "#ff1a1a", 
         align: "center"
         };
      //   this.tutorialText =  (this.game.add.text (this.game.world.centerX, this.game.height * 0.25, "Tap the screen", style));
    //    this.tutorialText.anchor.setTo (0.5, 0.5);
     //   this.container.add(this.tutorialText);
        this.labelScore.text = 0;  

        adaptGameToOrientation();
        assignOrientationChangeHandlers();
       
    }
    addOnePipe(x,y) 
    {
        // Create a pipe at the position x and y
        var pipe = game.add.sprite(x, y, 'pipe');
        pipe.scale.x *=2;
        pipe.scale.y *=2;
        // Add the pipe to our previously created group
        this.pipes.add(pipe);
    
        // Enable physics on the pipe 
        game.physics.arcade.enable(pipe);
    
        // Add velocity to the pipe to make it move left
        pipe.body.velocity.x = -200; 
    
        // Automatically kill the pipe when it's no longer visible 
        pipe.checkWorldBounds = true;
        pipe.outOfBoundsKill = true;

    }

    addRowOfPipes() {
        // Randomly pick a number between 1 and 5
        // This will be the hole position
        if(this.isDone) return;
        var hole = Math.floor(Math.random() * 5) + 1;
    
        // Add the 6 pipes 
        // With one big hole at position 'hole' and 'hole + 1'
        for (var i = 0; i < 12; i++)
            if (i != hole && i != hole + 1) 
                this.addOnePipe(this.pipeXAxis, i * (this.pipeAmount) + 10);  

                this.score+=1;
                this.labelScore.text = this.score;  
    }
    
    PointerTutorial()
    {
        if(this.hasGameStarted) 
        {
     
        //    this.characterSprite.body.onWorldBounds.add(this.restartGame, this);
        }
        if (!this.hasGameStarted)
        {
           this.InitializeStart();
            this.hasGameStarted = true;
      //      this.tutorialText.destroy();
            this.timer = game.time.events.loop(2500, this.addRowOfPipes, this); 

        }

        

    }
    InitializeStart()
    {
                // Initialize Controls
    
                    this.game.input.onDown.add(this.jump,this);

                    
          //Initialize Character Physics
          game.physics.enable(this.characterSprite, Phaser.Physics.ARCADE);
          this.characterSprite.body.enable=true;
          this.characterSprite.body.gravity.y = 1000;
          this.characterSprite.body.bounce.y = 0.5;
          this.characterSprite.body.setSize(600,400,200,300);
          this.characterSprite.body.collideWorldBounds=true;      
          this.jump();
          
       //   this.characterSprite.body.onWorldBounds = new Phaser.Signal();
       //   this.characterSprite.body.onWorldBounds.add(this.restartGame, this);
          this.isDone=false;
  
          
    }
    jump() 
    {
        // Add a vertical velocity to the bird
        this.characterSprite.body.velocity.y = -350;
    }
   
    gameStart () {
        // add a delay timer that will trigger game over
        game.time.events.add(Phaser.Timer.SECOND * 12, function () {
            this.gameOver ("win");
            this.characterSprite.body.enable=false;
            this.characterSprite.visible = false;
            this.labelScore.visible= false;
        //    this.tutorialText.visible=false;
        }, this);
    }

    gameOver (condition) {
        // hide the install button at the top
        this.installButton.visible = false;
        this.isDone=true;

        // create a transparent black background to dim the game
        this.showMenu ();

        // add the end card on top of the game
        this.container.addChild (this.endCard);

        // pass in the condition to the end card to modify the header text
        this.endCard.show (condition);

    }

    showMenu () {
        this.endCardPicture.visible = true;
    }

    reorientLayout (orientation) {
        this.orient = orientation;

        this.background.angle = this.config[orientation].background.angle;
        this.background.x = this.config[orientation].background.x;
        this.background.y = this.config[orientation].background.y;

        this.backgroundSprite.x = this.config[orientation].background.x;
        this.backgroundSprite.y = this.config[orientation].background.y;

        this.installButton.x = this.config[orientation].installButton.x;
        this.installButton.y = this.config[orientation].installButton.y;
        this.installButton.scale.x = this.config[orientation].installButton.scaleX;
        this.installButton.scale.y = this.config[orientation].installButton.scaleY;

        this.endCard.x = this.config[orientation].endCard.x;
        this.endCard.y = this.config[orientation].endCard.y;
        this.endCard.scale.x = this.config[orientation].endCard.scaleX;
        this.endCard.scale.y = this.config[orientation].endCard.scaleY;

        //Game Stuff
        this.characterSprite.x = this.config[orientation].characterSprite.x;
        this.characterSprite.y = this.config[orientation].characterSprite.y;
        this.characterSprite.scale.x = this.config[orientation].characterSprite.scaleX;
        this.characterSprite.scale.y = this.config[orientation].characterSprite.scaleY;

        this.pipeAmount = this.config[orientation].pipeAmount.x;
        this.pipeXAxis = this.config[orientation].pipeAmount.position;
        this.ground.y = this.config[orientation].ground.y;

        this.endCardPicture.x = this.config[orientation].endCardPicture.x;
        this.endCardPicture.y = this.config[orientation].endCardPicture.y;
        this.endCardPicture.scale.x = this.config[orientation].endCardPicture.scaleX;
        this.endCardPicture.scale.y = this.config[orientation].endCardPicture.scaleY;
       // this.pipe.x = this.config[orientation].pipe.x;
       // this.pipe.y = this.config[orientation].pipe.y;
       // this.pipe.scale.x = this.config[orientation].pipe.scaleX;
       // this.pipe.scale.y = this.config[orientation].pipe.scaleY;
       
    }
}
