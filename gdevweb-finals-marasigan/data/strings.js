strings = {
    "en": {
        "TEST": "Loading",
        "IS_MOBILE": "Loading your pizza...",
        "IS_DESKTOP": "Loading your pizza...",
        "ENDCARD_HEADER_WON": "YOU WON!",
        "ENDCARD_HEADER_LOST": "YOU LOST!",
        "ENDCARD_BODY": "Craving for\n pizza?",
        "INSTALL_NOW": "ORDER NOW"
    },
    "fil": {
        "TEST": "Kinakarga",
        "IS_MOBILE": "Sinusubukan sa Cellphone...",
        "IS_DESKTOP": "Sinusubukan sa Kompyuter...",
        "ENDCARD_HEADER_WON": "NANALO KA!",
        "ENDCARD_HEADER_LOST": "NATALO KA!",
        "ENDCARD_BODY": "Gusto mo ba\nng pizza?",
        "INSTALL_NOW": "Umorder na"
    }
}